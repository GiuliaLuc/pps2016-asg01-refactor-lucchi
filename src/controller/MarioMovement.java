package controller;

import game.Main;
import model.GameComponentImpl;
import model.character.BasicCharacter;
import model.character.Character;
import model.object.Piece;
import utilities.Res;
import utilities.Utils;

import java.awt.*;

/**
 * Created by lucch on 17/03/2017.
 */
public class MarioMovement extends CharacterIterationImpl {

    GameComponentMovement componentMovement;
    CharacterIteration characterIteration;
    Character character;

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int JUMPING_LIMIT = 42;

    private boolean jumping;
    private int jumpingExtent;

    public MarioMovement(Character character) {
        super(character);
        this.characterIteration = new CharacterIterationImpl(character);
        this.componentMovement = new GameComponentMovementImpl(character);

    }

    public Image doJump() {
        String imagePath;
        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (character.getY() > Main.scene.getHeightLimit())
                character.setY(character.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            imagePath = character.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (character.getY() + character.getHeight() < Main.scene.getFloorOffsetY()) {
            character.setY(character.getY() + 1);
            imagePath = character.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            imagePath = character.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(imagePath);
    }

    public void contact(GameComponentImpl gameComponent) {
        if (componentMovement.hitAhead(gameComponent) && character.isToRight() || componentMovement.hitBack(gameComponent) && !character.isToRight()) {
            Main.scene.setMov(0);
            character.setMoving(false);
        }

        if (componentMovement.hitBelow(gameComponent) && this.jumping) {
            Main.scene.setFloorOffsetY(gameComponent.getY());
        } else if (!componentMovement.hitBelow(gameComponent)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                character.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (componentMovement.hitAbove(gameComponent)) {
                Main.scene.setHeightLimit(gameComponent.getY() + gameComponent.getHeight()); // the new sky goes below the object
            } else if (!componentMovement.hitAbove(gameComponent) && !this.jumping) {
                Main.scene.setHeightLimit(0);
            }
        }
    }

    public boolean contactPiece(Piece piece) {
       return (componentMovement.hitBack(piece) || componentMovement.hitAbove(piece) || componentMovement.hitAhead(piece)
                || componentMovement.hitBelow(piece));
    }

    public void contactCharacter(BasicCharacter otherCharacter) {
        if (componentMovement.hitAhead(otherCharacter) || componentMovement.hitBack(otherCharacter)) {
            if (otherCharacter.getAlive()) {
                character.setMoving(false);
                character.setAlive(false);
            } else {
                otherCharacter.setAlive(true);
            }
        } else if (componentMovement.hitBelow(otherCharacter)) {
            otherCharacter.setMoving(false);
            otherCharacter.setAlive(false);
        }
    }

}
