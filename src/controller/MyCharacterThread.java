package controller;

import model.character.Character;

/**
 * Created by lucch on 17/03/2017.
 */
public class MyCharacterThread implements Runnable {

    private static final int PAUSE = 15;

    Character character;
    CharacterIteration iteration = new CharacterIterationImpl(character);

    @Override
    public void run() {
        while (true) {
            if (this.character.getAlive()) {
                this.iteration.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

}
