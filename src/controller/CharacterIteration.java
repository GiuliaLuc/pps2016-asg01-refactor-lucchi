package controller;

import model.GameComponentImpl;

/**
 * Created by lucch on 17/03/2017.
 */
public interface CharacterIteration {

    void contact(GameComponentImpl gameComponent);

    void move();

}
