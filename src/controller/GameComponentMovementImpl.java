package controller;

import model.GameComponent;
import model.character.Character;
import game.Main;
import model.GameComponentImpl;
import utilities.Res;
import utilities.Utils;

import java.awt.*;

import static model.character.BasicCharacter.PROXIMITY_MARGIN;

/**
 * Created by lucch on 15/03/2017.
 */
public class GameComponentMovementImpl implements GameComponentMovement {

    private Character character;

    public GameComponentMovementImpl(Character character){
        this.character=character;
    }

    @Override
    public Image walk(String name, int frequency) {
        character.setCounter(character.getCounter()+1);
        String str = Res.IMG_BASE + name + (!character.isMoving()|| character.getCounter() % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (character.isToRight() ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    @Override
    public void move() {
        if (Main.scene.getxPos() >= 0) {
            character.setX(character.getX() - Main.scene.getMov());
        }
    }

    @Override
    public boolean hitAheadCharacter(GameComponent character) {
        return !((this.character.getX() + this.character.getWidth() < character.getX() || this.character.getX() + this.character.getWidth() > character.getX() + 5 ||
                this.character.getY() + this.character.getHeight() <= character.getY() || this.character.getY() >= character.getY() + character.getHeight()));
    }

    @Override
    public boolean hitAhead(GameComponent gameComponent) {
        if (this.character.isToRight()) {
            return this.hitAheadCharacter(gameComponent);
        }
        return false;
    }

    @Override
    public boolean hitBack(GameComponent gameComponent) {
        return !(this.character.getX() > gameComponent.getX() + gameComponent.getWidth() || this.character.getX() + this.character.getWidth() < gameComponent.getX() + gameComponent.getWidth() - 5 ||
                this.character.getY() + this.character.getHeight() <= gameComponent.getY() || this.character.getY() >= gameComponent.getY() + gameComponent.getHeight());

    }

    @Override
    public boolean hitBelow(GameComponent gameObject) {
        return !((character.getX() + character.getWidth() < gameObject.getX() + 5 || character.getX() > gameObject.getX() + gameObject.getWidth() - 5 ||
                character.getY() + character.getHeight() < gameObject.getY() || character.getY() + character.getHeight() > gameObject.getY() + 5));
    }

    @Override
    public boolean hitAbove(GameComponentImpl gameObject) {
        return !((character.getX() + character.getWidth() < gameObject.getX() + 5 || character.getX() > gameObject.getX() + gameObject.getWidth() - 5 ||
                character.getY() < gameObject.getY() + gameObject.getHeight() || character.getY() > gameObject.getY() + gameObject.getHeight() + 5));
    }

    @Override
    public boolean isNearby(GameComponent gameComponent) {
        return ((character.getX() > gameComponent.getX() - PROXIMITY_MARGIN && character.getX() < gameComponent.getX() + gameComponent.getWidth() + PROXIMITY_MARGIN)
                || (character.getX() + character.getWidth() > gameComponent.getX() - PROXIMITY_MARGIN && character.getX() + character.getWidth() < gameComponent.getX() + gameComponent.getWidth() + PROXIMITY_MARGIN));
    }

}
