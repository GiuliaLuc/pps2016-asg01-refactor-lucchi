package controller;

import model.GameComponent;
import model.character.Character;
import model.GameComponentImpl;

import java.awt.*;

import static model.character.BasicCharacter.PROXIMITY_MARGIN;

/**
 * Created by lucch on 15/03/2017.
 */
public interface GameComponentMovement {

    Image walk(String name, int frequency);

    void move();

    boolean hitAheadCharacter(GameComponent character);

    boolean hitAhead(GameComponent gameComponent);

    boolean hitBack(GameComponent gameComponent);

    boolean hitBelow(GameComponent gameObject);

    boolean hitAbove(GameComponentImpl gameObject);

    boolean isNearby(GameComponent gameComponent);

}
