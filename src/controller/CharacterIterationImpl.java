package controller;

import model.GameComponentImpl;
import model.character.Character;

/**
 * Created by lucch on 17/03/2017.
 */
public class CharacterIterationImpl implements CharacterIteration{

    Character character;
    GameComponentMovement movement;
    boolean dxCharacter;

    public CharacterIterationImpl(Character character) {
        this.character = character;
        movement = new GameComponentMovementImpl(character);
        this.dxCharacter=false;

    }

    @Override
    public void contact(GameComponentImpl gameComponent) {
        if (movement.hitAhead(gameComponent) && character.isToRight()) {
            character.setToRight(false);
            this.dxCharacter=false;
        } else if (movement.hitBack(gameComponent) && !character.isToRight()) {
            character.setToRight(true);
            this.dxCharacter=true;
        }
    }

    @Override
    public void move() {
        this.dxCharacter = character.isToRight() ? true : false;
        this.character.setX(this.character.getX() + (this.dxCharacter ? 1 : -1));
    }

}
