package model;

import java.awt.*;

/**
 * Created by lucch on 15/03/2017.
 */
public interface GameComponent {

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    Image getImage();

    void setX(int x);

    void setY(int y);

    void setImage(String pathImage);

   // void move();
}
