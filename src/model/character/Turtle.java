package model.character;

import java.awt.Image;

import model.character.BasicCharacter;
import model.character.Character;
import model.GameComponentImpl;
import utilities.Res;
import utilities.Utils;

public class Turtle extends BasicCharacter {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT, Res.IMG_TURTLE_IDLE);
        super.setToRight(true);
        super.setMoving(true);
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
