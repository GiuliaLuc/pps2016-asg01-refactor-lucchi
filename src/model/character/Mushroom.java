package model.character;

import java.awt.Image;

import model.character.BasicCharacter;
import model.character.Character;
import model.GameComponentImpl;
import utilities.Res;
import utilities.Utils;

public class Mushroom extends BasicCharacter {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;


    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT, Res.IMG_MUSHROOM_DEFAULT);
        this.setToRight(true);
        this.setMoving(true);
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
