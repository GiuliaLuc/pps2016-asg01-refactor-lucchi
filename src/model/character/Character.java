package model.character;

import model.GameComponent;

import java.awt.Image;

public interface Character extends GameComponent {

	int getCounter();

    boolean getAlive();

	boolean isMoving();

	boolean isToRight();

	void setAlive(boolean alive);


	void setMoving(boolean moving);

	void setToRight(boolean toRight);

	void setCounter(int counter);

	//da mettere nel controller
	//Image walk(String name, int frequency);
}
