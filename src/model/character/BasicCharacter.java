package model.character;

import controller.MyCharacterThread;
import model.GameComponentImpl;

public class BasicCharacter  extends GameComponentImpl implements Character {

    public static final int PROXIMITY_MARGIN = 10;

    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;
    private String imagePath;


    public BasicCharacter(final int x, final int y, final int width, final int height, final String imagePath) {
        super(x,y,width,height, imagePath);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
        this.imagePath=imagePath;

        MyCharacterThread chronoCharacter = new MyCharacterThread();
        Thread thread = new Thread(chronoCharacter);
        thread.start();
    }

    @Override
    public int getCounter() {
        return counter;
    }

    @Override
    public boolean getAlive() {
        return alive;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public boolean isToRight() {
        return toRight;
    }

    @Override
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public void setCounter(int counter) {
        this.counter = counter;
    }

}



