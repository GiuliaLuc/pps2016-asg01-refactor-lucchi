package model;

import java.awt.Image;

import utilities.Utils;

public class GameComponentImpl implements GameComponent {

    private final int width;
    private final int height;
    private int x;
    private int y;

    private String imagePath;
    private Image imageCharacter;

    public GameComponentImpl (final int x, final int y, final int width, final int height, final String imagePath) {
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.imagePath=imagePath;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }


    @Override
    public Image getImage() {
        return this.imageCharacter;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setImage(String pathImage) {
        this.imageCharacter = Utils.getImage(pathImage);
    }


    /** //controller
    public void move() {
        if (Main.scene.getxPos() >= 0) {
            this.x = this.x - Main.scene.getMov();
        }
    }
*/
}
