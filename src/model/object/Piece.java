package model.object;

import model.GameComponentImpl;
import utilities.Res;
import utilities.Utils;

import java.awt.Image;

public class Piece extends GameComponentImpl implements Runnable {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    public static final int FLIP_FREQUENCY = 100;
    private int counter;

    public Piece(int x, int y) {
        super(x, y, WIDTH, HEIGHT, Res.IMG_PIECE1);
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
