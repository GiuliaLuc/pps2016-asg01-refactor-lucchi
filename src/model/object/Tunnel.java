package model.object;

import model.GameComponentImpl;
import utilities.Res;
import utilities.Utils;

public class Tunnel extends GameComponentImpl {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 65;

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT, Res.IMG_TUNNEL);
    }
}
