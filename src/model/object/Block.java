package model.object;

import model.GameComponentImpl;
import utilities.Res;
import utilities.Utils;

public class Block extends GameComponentImpl {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT, Res.IMG_BLOCK);
    }

}
